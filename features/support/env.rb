# See http://wiki.github.com/aslakhellesoy/cucumber/sinatra
# for more details about Sinatra with Cucumber

require 'test/unit'
require 'rack/test'
require 'webrat'

require File.join( File.dirname(__FILE__), '..', '..', 'application' )

Webrat.configure do |config|
  config.mode = :rack
end

World do
  def app
    Sinatra::Application
  end

  include Test::Unit::Assertions
  include Rack::Test::Methods
  include Webrat::Methods
  include Webrat::Matchers
end
