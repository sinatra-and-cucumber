Given /^I am viewing "([^\"]*)"$/ do |url|
  visit url
end

Then /^I should see "(.+)"$/ do |text|
  assert response_body =~ Regexp.new( Regexp.escape(text) )
end

Then /^I should be happy$/ do
  assert_equal 200, last_response.status
end
