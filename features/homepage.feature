Feature: View Homepage
  In order to test the application
  As a developer
  I want to see "Hello World" on homepage

  Scenario: Home page
    Given I am viewing "/"
    Then I should see "Hello, World!"
    And I should be happy
