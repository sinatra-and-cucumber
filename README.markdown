Cucumber and Sinatra demonstration
==================================

Simplistic example to demonstrate Cucumber acceptance testing via plain-text feature stories.

Available at http://repo.or.cz/w/sinatra-and-cucumber.git?a=log

Run:

    $ ruby application.rb
    $ rake

---

Copyright (c) 2008 Karel Minarik (www.karmi.cz), licensed under MIT License